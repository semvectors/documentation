.. Documentationdocenc documentation master file, created by
   sphinx-quickstart on Thu Apr  6 02:09:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Библиотека ExactusSemVectors для формирования кросс-языковых векторных представлений текстов.
:doc:`Обзор <lib_guide>` основных возможностей библиотеки.

Быстрое начало работы
=====================

Скачайте небольшой набор документов и предобученную модель:

.. code-block:: console

  $ curl -O dn11.isa.ru:8080/doc-enc-data/datasets.docs.mini.v1.tar.gz
  $ tar xf datasets.docs.mini.v1.tar.gz
  $ find docs-mini/texts/ -name "*.txt" > files.txt

  $ curl -O http://dn11.isa.ru:8080/doc-enc-data/models.def.pt

Данные доступны для скачивания по `ссылке <https://mega.nz/folder/vkghwIDY#3WIHndJIiti5HHrncR8IVg>`_, в случае если приведенные выше ссылки не работают.

Для запуска процесса конвертации удобно использовать готовый докер-образ.
Перед этим необходимо установить `nvidia container toolkit <https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#setting-up-nvidia-container-toolkit>`_ для докера.

.. code-block:: console

  $ docker run  --gpus=1  --rm  -v $(pwd):/temp/ -w /temp  \
    semvectors/doc_enc:0.1.2 \
    docenccli docs -i /temp/files.txt -o /temp/vecs -m /temp/models.def.pt

Вектора будут сохранены в директории ``vecs`` вместе с именами файлов, с помощью функции ``numpy.savez``.
Пример загрузки векторов из этих файлов:

.. code-block:: python

  import numpy as np

  obj = np.load('vecs/0000.npz')
  print(obj['ids'][:2])
  print(obj['embs'][:2])


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   lib_guide
   models
   inference
   fine_tuning
   training
   eval
   nix
   preparation

Благодарности
"""""""""""""
Библиотека создана при поддержке Фонда содействия инновациям" (Договор № 6ГУКодИИС12-D7/72695 о предоставлении гранта на выполнение проекта открытых библиотек от 26 декабря 2021 г.)
