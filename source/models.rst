
Предобученные модели
====================

* `doc_trans_full1-9-4 <http://dn11.isa.ru:8080/doc-enc-data/models.doc_trans_full1-9-4.pt>`_
   * 3xLSTM + 2xTransformer + 3xTransformer;
   * Training data - SimEnWiki,SimRuWiki,ParalWiki,SimEnSci;
   * 2xA100, 240k updates

Также модели можно скачать по `ссылке <https://mega.nz/folder/vkghwIDY#3WIHndJIiti5HHrncR8IVg>`_.

**Благодарности**

Обучение моделей выполнялось с использованием инфраструктуры Центра коллективного пользования «Высокопроизводительные вычисления и большие данные» (ЦКП «Информатика») ФИЦ ИУ РАН (г. Москва).

Предыдущие версии модели
------------------------

* `doc_trans_full1-2 <http://dn11.isa.ru:8080/doc-enc-data/models.doc_trans_full1-2.pt>`_, `ts версия <http://dn11.isa.ru:8080/doc-enc-data/models.doc_trans_full1-2.ts.pt>`_
   * 3xLSTM + 2xTransformer + 3xTransformer;
   * Training data - SimEnWiki,SimRuWiki,ParalWiki;
   * 2xA100, 80k updates

* `doc_trans_full1-8-4 <http://dn11.isa.ru:8080/doc-enc-data/models.doc_trans_full1-8-4.pt>`_
   * 3xLSTM + 2xTransformer + 3xTransformer;
   * Training data - SimEnWiki,SimRuWiki,ParalWiki,SimEnSci;
   * 2xA100, 240k updates
