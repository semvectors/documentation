Работа в среде nix
==================

Предварительная настройка
-------------------------

 * Установите nix в режиме `Single-user install <https://nixos.wiki/wiki/Nix_Installation_Guide>`_ версии 2.8 или более новый.
 * Добавьте конфигурационный файл ``~/.config/nix/nix.conf``:
        ::

                experimental-features = nix-command flakes
                extra-substituters =  https://is2.isa.ru:5002/semvectors
                trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= semvectors:5YozS85voLSajjzsSlT9Y6pDyiVnjm7kFlRWmPx6l5k=
                bash-prompt-prefix = dev_


Установка библиотеки
--------------------

Установки библиотеки и всех зависимостей:

.. code-block:: console

  $ nix shell git+https://gitlab.com/semvectors/doc_enc

Эта команда скачает и при необходимости соберет все зависимости и создаст окружение, в котором доступны утилиты командной строки, поставляемые с библиотекой.

В случае если, планируется запускать обучение модели, нужно установить зависимости для обучения.

.. code-block:: console

  $ nix shell git+https://gitlab.com/semvectors/doc_enc#train


Использование GPU
"""""""""""""""""
Чтобы библиотека могла использовать GPU необходимо выставить переменную окружения с путем к директории, в которой лежит ``libcuda.so`` (разделяемая библиотека, поставляемая вместе с драйвером к GPU).
В ОС Debian эта библиотека обычно находится в директории ``/usr/lib/x86_64-linux-gnu/nvidia/current/``.
Найти библиотеку можно с помощью команды ``find  /usr/lib /lib -wholename "*64*libcuda.so"`` или ``find  /usr/lib /lib   -name "libcuda.so"``.
Если было найдено несколько библиотек, то необходимо выбрать библиотеку,
соответствующую архитектуре платформы (посмотреть архитектуры платформы можно с помощью команды ``arch``).
Чтобы избежать ошибки::

  UserWarning: User provided device_type of 'cuda', but CUDA is not available

Необходимо каждый раз после старта ``nix shell`` выполнять команду:

.. code-block:: console

  $ export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/nvidia/current/libcuda.so

Заменив на актуальный путь до ``libcuda.so``.

Либо можно выкачать `репозиторий <https://gitlab.com/semvectors/doc_enc>`_ и в нем выполнить команду
``nix develop``.
Тогда значение  ``LD_LIBRARY_PATH`` будет изменяться автоматически.
Изменить значение пути можно в файле ``flake.nix`` в директиве ``shellHook``.


Работа с библиотекой в командной строке
---------------------------------------

Преобразование документов из файловой системы
"""""""""""""""""""""""""""""""""""""""""""""

Все тексты должны быть предварительно :ref:`сегментированы<inference:пример сегментации документов>`.
Предполагается, что пути к текстам для преобразования собраны в файле ``files.txt``.
Для старта преобразования выполните следующую команду:

.. code-block:: console

  $ docenccli docs -i files.txt -o vecs -m models.def.pt

где `models.def.pt <http://dn11.isa.ru:8080/doc-enc-data/models.def.pt>`_ одна из предобученных моделей.
:ref:`Формат результатов преобразования<inference:результаты преобразования>`

:ref:`inference:основные опции docenccli`

Запуск обучения
"""""""""""""""

Требуется выкачать конфигурацию для обучения из репозитория
`conf <https://gitlab.com/semvectors/doc_enc/-/tree/master/train/conf>`_.
Необходимо подготовить персональный конфигурационный файл по аналогии с
`файлом <https://gitlab.com/semvectors/doc_enc/-/blob/master/train/conf/personal/dvzubarev/tsa06.yaml>`_.
В нем прописать локальные пути для модели
:ref:`токенизатора<preparation:токенизатор>`,
датасета с
:ref:`предложениями<preparation:Параллельные предложения>`
/
:ref:`документами<preparation:Датасет похожих документов>`.
Для тестирования удобно использовать сэмплы датасетов:

* `Токенизатор <http://dn11.isa.ru:8080/doc-enc-data/tokenizer.bpe.256k_from_600M.spm.model>`_.
* `Предложения <http://dn11.isa.ru:8080/doc-enc-data/datasets.sents.mini.v1.tar.gz>`_.
* `Документы <http://dn11.isa.ru:8080/doc-enc-data/datasets.docs.mini.v1.tar.gz>`_.

Запустить обучение можно с помощью команды:

.. code-block:: console

  $ run_training --config-path $(pwd)/conf +personal/dvzubarev=tsa06

указав свой персональный конфиг.
Для обучения требуется наличие GPU.
Модели и логи будут сохранены в папку ``train/outputs``, в которой будет создаваться рабочая директория для каждого запуска обучения.

**Оценка качества**

Для запуска оценки качества обученных моделей нужно подготовить
`конфигурацию <https://gitlab.com/semvectors/doc_enc/-/tree/master/eval/conf>`_ и
:ref:`данные <eval:тестовые данные>`.

Необходимо подготовить персональный конфигурационный файл по аналогии с
`файлом <https://gitlab.com/semvectors/doc_enc/-/blob/master/eval/conf/personal/dvzubarev/tsa06-quick.yaml>`_.
В нем прописать локальные пути для модели и тестовых данных.
Команда запуска обучения:

.. code-block:: console

   $ run_eval --config-path $(pwd)/conf +personal/dvzubarev=tsa06-quick

указав свой персональный конфиг.

:ref:`eval:основные параметры оценки качества`

Запуск настройки
""""""""""""""""

Для запуска нужно подготовить
:ref:`данные <fine_tuning:формат датасета>` и
`конфигурацию <https://gitlab.com/semvectors/doc_enc/-/tree/master/finetune/conf>`_.

Необходимо подготовить персональный конфигурационный файл по аналогии с
`файлом <https://gitlab.com/semvectors/doc_enc/-/blob/master/finetune/conf/personal/dvzubarev/classif_tsa06.yaml>`_.
В нем прописать локальные пути для модели и обучающих данных.
Команда запуска обучения:

.. code-block:: console

  $ fine_tune_classif --config-path $(pwd)/conf +personal/dvzubarev=classif_tsa06

указав свой персональный конфиг.

:ref:`fine_tuning:Основные параметры настройки модели`.


Использование API библиотеки
----------------------------

Для того чтобы использовать API библиотеки, необходимо выкачать репозиторий и выполнить команду ``nix develop``.

Создайте файл :ref:`enc.py <inference:преобразование документов с помощью программного интерфейса>`, использующий API.

.. code-block:: console

  $ python enc.py

:ref:`inference:описание api`

Создание нового проекта с зависимостью doc_enc
""""""""""""""""""""""""""""""""""""""""""""""

Для создания нового проекта необходимо выполнить:

.. code-block:: console

  $ git init proj
  $ vim proj/flake.nix
  $ cd proj
  $ git add flake.nix
  $ nix develop

Содержимое ``flake.nix``:

.. code-block:: nix

  {
  description = "Description";

  inputs.doc_enc.url = "git+https://gitlab.com/semvectors/doc_enc";
  inputs.nixpkgs.follows = "doc_enc/nixpkgs";
  outputs = { self, nixpkgs, doc_enc }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ doc_enc.overlays.default ];
        config = {allowUnfree = true;};
      };
      pypkgs = pkgs.python.pkgs;

      py-env = pkgs.python.buildEnv.override{
        extraLibs = [
          pypkgs.doc_enc
        ];
      };
    in {

      devShells.x86_64-linux = {
        default = pkgs.mkShell {
          buildInputs = [
            py-env
            pkgs.nodePackages.pyright
            pypkgs.pylint
            pypkgs.black
          ];

          shellHook=''
            export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/nvidia/current/
          '';
        };
      };
    };

  }


После этого в новом проекте можно создавать скрипты, использующие doc_enc, например
:ref:`enc.py <inference:преобразование документов с помощью программного интерфейса>`.

.. code-block:: console

  $ python enc.py


Среда разработки библиотеки
---------------------------
Для того, чтобы попасть в окружение с установленными зависимостями и инструментами разработки нужно выкачать проект и в корневой папке проекта выполнить ``nix develop``.
Настройка инструментов разработки (pyright, pylint) осуществляется с помощью direnv
(можно поставить с помощью ``nix-env -f '<nixpkgs>' -iA direnv``) и nix-direnv ( `Установка <https://github.com/nix-community/nix-direnv#installation>`_ ).
При условии что в вашем редакторе есть интеграция с direnv, библиотека и зависимости должны быть обнаружены.
